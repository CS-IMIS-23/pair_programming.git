
import java.util.ArrayList;
import java.util.Random;

public class Arithmetic {

    char[] operator = {'+', '-', '*', '÷'};
    Random generator = new Random();
    int num1, num2, num3,num4, a;
    private String result="";
    ArrayList<String> questionName = new ArrayList<>();

    //  输出1级题目：+、-
    public String level1() {
        num1 = generator.nextInt(20)+1;
        num2 = generator.nextInt(20)+1;
        num3 = generator.nextInt(2);
        a = generator.nextInt(1);
        result = num1 + " " + operator[num3] + " " + num2;
        for (int x = 0; x <= a; x++) {
            num2 = generator.nextInt(20)+1;
            num3 = generator.nextInt(2);
            result += " " + operator[num3] + " " + num2;
        }
        return result;
    }

    //  输出2级题目：*、÷
    public String level2() {
        num1 = generator.nextInt(20)+1;
        num2 = generator.nextInt(20)+1;
        num3 = generator.nextInt(2) + 2;
        a = generator.nextInt(3);
        result = num1 + " " + operator[num3] + " " + num2;
        for (int x = 0; x <= a; x++) {
            num2 = generator.nextInt(20)+1;
            num3 = generator.nextInt(2) + 2;
            result += " " + operator[num3] + " " + num2;
        }
        return result;
    }

    //  输出3级题目：+、-、*、÷
    public String level3() {
        num1 = generator.nextInt(20)+1;
        num2 = generator.nextInt(20)+1;
        num3 = generator.nextInt(4);
        result = num1 + " "+ operator[num3] + " " + num2;
        a = generator.nextInt(1)+1;
        for (int x = 0; x <= a; x++) {
            num3 = generator.nextInt(4);
            num4 = generator.nextInt(2);
            num2 = generator.nextInt(20)+1;

            if(a!=0)
            {
                result += " " + operator[generator.nextInt(2)] ;
            }
            a--;
            result += " "+Arithmetic.bracket() + " " + operator[num3]+ " " + num2 +" "+operator[num3]+" "+Arithmetic.bracket();


    } return result;
    }


    //生成分数.
    public RationalNumber fraction() {
        int numer = generator.nextInt(20)+1;
        int denom = generator.nextInt(20)+1;
        RationalNumber a = new RationalNumber(numer, denom);
        if (numer > denom) {
            if (numer % denom == 0) {
                return a;
            } else {
                a = a.reciprocal();
            }
        } else {
            return a;
        }
        return a;
    }

    //四级题目
    public String level4() {
        Arithmetic[] fraction = new Arithmetic[5];
        RationalNumber[] fraction1 = new RationalNumber[5];
        for (int x = 0; x < fraction.length; x++) {
            Arithmetic a = new Arithmetic();
            fraction1[x] = a.fraction();
        }
        num3 = generator.nextInt(4);
        a = generator.nextInt(2);
        result = fraction1[0] + " " + operator[num3] + " " + fraction1[1];
        if (a > 2) {
            for (int x = 2; x < a; x++) {
                num3 = generator.nextInt(4);
                result += " " + operator[num3] + " " + fraction1[x];
            }
        }
        return result;
    }

    //  重写toString方法
    public String toString(int number) {
        String result1="";

        result1=  "问题" + number + ": " + result + " = ";

        return result1;
    }
    @Override
    public String toString(){
        String result = "";
        result = this.result;
        return result;
    }
    //生成括号的方法。




    public static String bracket()
    {
        char[] operator = {'+', '-', '*', '÷'};
        Random generator = new Random();
        int num1, num2, num3, num4,a;
        String result1;
        String ab,ab1,ab2,ab3,ab4,ab5,ab6,ab7;
        String []abc=new String [9];
        num1 = generator.nextInt(20) + 1;
        num2 = generator.nextInt(20) + 1;
        num3 = generator.nextInt(4);
        num4 = generator.nextInt(2);
        ab = " "+ operator[num3]+" ( "+ (generator.nextInt(20)+1) +" "+ operator[num4]+ " "+(generator.nextInt(20)+1)+" ) ";
        ab1 = " "+ operator[num3]+" ( "+ (generator.nextInt(20)+1) +" "+ operator[num4]+ " "+(generator.nextInt(20)+1)+" ) ";
        ab2 = " "+ operator[num3]+" ( "+ (generator.nextInt(20)+1) +" "+ operator[num4]+ " "+(generator.nextInt(20)+1)+" ) ";
        ab3 = " "+ operator[num3]+" ( "+ (generator.nextInt(20)+1) +" "+ operator[num4]+ " "+(generator.nextInt(20)+1)+" ) ";
        ab4 = " "+ operator[num3]+" ( "+ (generator.nextInt(20)+1) +" "+ operator[num4]+ " "+(generator.nextInt(20)+1)+" ) ";
        ab5 = " "+ operator[num3]+" ( "+ (generator.nextInt(20)+1) +" "+ operator[num4]+ " "+(generator.nextInt(20)+1)+" ) ";
        ab6 = " "+ operator[num3]+" ( "+ (generator.nextInt(20)+1) +" "+ operator[num4]+ " "+(generator.nextInt(20)+1)+" ) ";
        ab7= " "+ operator[num3]+" ( "+ (generator.nextInt(20)+1) +" "+ operator[num4]+ " "+(generator.nextInt(20)+1)+" ) ";
        abc[1]=ab;
        abc[2]=ab1;
        abc[3]=ab2;
        abc[4]=ab3;
        abc[5]=ab4;
        abc[6]=ab5;
        abc[7]=ab6;
        abc[8]=ab7;
        abc[0]=" ";
        result1 = num1 +abc[generator.nextInt(9)] + operator[num3]+" "+num2;

        return result1;
    }
//    public void
}