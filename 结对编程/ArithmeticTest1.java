import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;
public class ArithmeticTest1 {
    public static void main(String []args) {
        Convert bee=new Convert();
        Arithmetic num = new Arithmetic();
        Comparision ben=new Comparision();
        DecimalFormat fmt=new DecimalFormat("0.00");
        NumberFormat fmt1= NumberFormat.getPercentInstance();
        int a;//用户输入的等级数。
        int b;//用户想要做的题目数量。
        ;//用户算出所给题目的答案。
        String c1;//专门为level4设置的字符串类型的输入。
        int d;//用户作对的题目数。
        int e=0;//用户答对的题目数.
        int ant=1;//题目编号数字.

        double f=0;//求出来正确率的小数数形式。
        double h;//一共做的题目数量变成double型。
        double i;//用户做对的题目数量变成double型。
        String another;////让用户输出y/n的Scanner类型。
        String lastexpression;//后缀表达式字符串型。;
        String luna;//转化成的正确率百分数模式。
        String ann;
        double abc;//将字符串小数点型正确率变成double型数字。

        //            paring abb=new paring();
        Scanner scan = new Scanner(System.in);
        System.out.println("欢迎使用四则运算题库！");
        do{
            System.out.println("请选择题目难度等级(例如:1,2,3,4):");
            a = scan.nextInt();
            System.out.println("请选择题目数量：");
            b = scan.nextInt();
            e=0;
            ant=1;
            switch (a) {
                case 1:
                    for (int x = 0; x < b; x++) {

                        num.level1();
                        c1=scan.next();
                        System.out.print(num.toString(ant));

                        ant++;
//                            ann=abb.mmm(num.toString());
                        //                    if()如果正确，输出恭喜答对，d++。否则输出正确答案。
                        lastexpression= bee.transit(num.toString());
                        if (ben.calculator(lastexpression).equals(c1))
                        {System.out.println("恭喜答对");
                            e++;}
                        else
                            System.out.println("很遗憾，答错了"+"正确答案是"+ben.calculator(lastexpression));
                    }
                    h=(double)b;
                    i=(double)e;
                    f=i/h;
                    luna= fmt.format(f);
                    abc= Double.valueOf(luna);//把一个字符串表示的数字变成真正的数字.

                    System.out.println("正确率是"+fmt1.format(abc));
                    System.out.println();//给题目换行
                    break;
                case 2:
                    for (int x = 0; x < b; x++) {
                        num.level2();
                        System.out.print(num.toString(ant));
                        c1 = scan.next();
                        ant++;
                        //                    if()如果正确，输出恭喜答对，d++。否则输出正确答案。
                        lastexpression= bee.transit(num.toString());
                        if(ben.calculator(lastexpression).equals(c1))
                        {System.out.println("恭喜答对");
                            e++;}
                        else
                            System.out.println("很遗憾，答错了"+"正确答案是"+ben.calculator(lastexpression));
                    }
                    h=(double)b;
                    i=(double)e;
                    f=i/h;
                    luna= fmt.format(f);
                    abc= Double.valueOf(luna);

                    System.out.println("正确率是"+fmt1.format(abc));
                    System.out.println();//给题目换行
                    break;
                case 3:
                    for (int x = 0; x < b; x++) {
                        num.level3();
                        System.out.print(num.toString(ant));
                        c1 = scan.next();
                        ant++;
                        //                    if()如果正确，输出恭喜答对，d++。否则输出正确答案。
                        lastexpression= bee.transit(num.toString());
                        if (ben.calculator(lastexpression).equals(String.valueOf(c1)))
                        {System.out.println("恭喜答对");
                            e++;}
                        else
                            System.out.println("很遗憾，答错了"+"正确答案是"+ben.calculator(lastexpression));
                    }
                    h=(double)b;
                    i=(double)e;
                    f=i/h;
                    luna= fmt.format(f);
                    abc= Double.valueOf(luna);

                    System.out.println("正确率是"+fmt1.format(abc));
                    System.out.println();//给题目换行
                    break;
                case 4:
                    for (int x = 0; x < b; x++) {
                        num.level4();
                        System.out.print(num.toString(ant));
                        c1 = scan.next();
                        ant++;
                        //                    if()如果正确，输出恭喜答对，d++。否则输出正确答案。
                        lastexpression= bee.transit(num.toString());
                        if (ben.calculator(lastexpression).equals(String.valueOf(c1)))
                        {System.out.println("恭喜答对");
                            e++;}
                        else
                            System.out.println("很遗憾，答错了"+"正确答案是"+ben.calculator(lastexpression));
                    }
                    h=(double)b;
                    i=(double)e;
                    f=i/h;
                    luna= fmt.format(f);
                    abc= Double.valueOf(luna);

                    System.out.println("正确率是"+fmt1.format(abc));
                    System.out.println();//给题目换行
                    break;

                default:
                    System.out.println("请检查您的输入.你输入的结果超出范围了.");
            }
            System.out.println("要继续做题吗？(y/n)?");
            another = scan.next();
        }
        while (another.equalsIgnoreCase("y"));
        System.out.println();
    }
}

