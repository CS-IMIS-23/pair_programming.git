

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.StringTokenizer;

public class Convert {
    List<String> expre;   // expression 表达式
    Stack<String> op;   // 操作符
    String result = "";

    public Convert() {
        expre = new ArrayList<String>();
        op = new Stack();
    }

    // 中缀表达式转后缀表达式的转换方法。
    public String transit(String q)  // question
    {
        String a;           // a 是字符
        StringTokenizer tokenizer = new StringTokenizer(q);

        while (tokenizer.hasMoreTokens()) {
            a = tokenizer.nextToken();

            if (a.equals("("))
                op.push(a);         // 如果是"("，入栈
            else if (a.equals("+") || a.equals("-")) {
                if (!op.isEmpty()) {
                    if (op.peek().equals("("))
                        op.push(a);     // 如果栈顶"("，运算符入栈
                    else {
                        expre.add(op.pop());        // 先移除栈顶元素到列表中，再将运算符入栈
                        op.push(a);
                    }
                } else {
                    op.push(a);     // 栈为空，运算符入栈
                }
            } else if (a.equals("*") || a.equals("÷")) {
                if (!op.isEmpty()) {
                    if (op.peek().equals("*") || op.peek().equals("÷")) {
                        expre.add(op.pop());
                        op.push(a);
                    } else
                        op.push(a);
                } else
                    op.push(a); // 如果栈为空，运算符入栈
            } else if (a.equals(")")) {
                while (true) {
                    String b = op.pop();
                    if (!b.equals("(")) {
                        expre.add(b);
                    } else {
                        break;
                    }
                }
            } else {
                expre.add(a);  // 如果为操作数，入列表
            }
        }
        while (!expre.isEmpty() && !op.isEmpty()) {
            expre.add(op.pop());
        }

        for (String x : expre)
            result += x + " ";
        return result;
    }
}


