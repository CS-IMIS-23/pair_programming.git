
import junit.framework.TestCase;

public class ConvertTest extends TestCase {
    Convert a = new Convert();
    Convert b = new Convert();

    public void testTransit() {
        assertEquals("1 2 3 * + ", a.transit("1 + 2 * 3"));
        assertEquals("1 2 3 + * 4 + ", b.transit("1 * ( 2 + 3 ) + 4"));
    }
}
