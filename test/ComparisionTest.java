

import junit.framework.TestCase;
import org.junit.Test;

public class ComparisionTest extends TestCase {
    Comparision a = new Comparision();
    Comparision b = new Comparision();

    @Test
    public void testCalculator() {
        assertEquals(new RationalNumber(5, 1).toString(),a.calculator("1 2 3 + *"));
        assertEquals(new RationalNumber(-9, 1).toString(),b.calculator("6 7 8 + -"));
    }



    public void testConToRantionalNum() {
        assertEquals(new RationalNumber(2, 3).toString(), a.conToRantionalNum("2/3").toString());
    }

    public void testCalculate() {
        assertEquals(new RationalNumber(6, 1).toString(), a.calculate(new RationalNumber(2,1),'*', new RationalNumber(3,1)));
        assertEquals(new RationalNumber(2,3).toString(), b.calculate(new RationalNumber(2,1),'÷', new RationalNumber(3,1)));
    }
}

